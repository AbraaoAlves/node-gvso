#!/usr/bin/env node

'use strict';

var verbose = process.argv.indexOf('--verbose') !== -1;
var insane = process.argv.indexOf('--insane') !== -1;

if (verbose || insane) {
    process.env.GVSO_VERBOSE = true;
}

if (insane) {
    process.env.GVSO_VERBOSE_INSANE = true;
}

require('../lib/cmd.js').run();
