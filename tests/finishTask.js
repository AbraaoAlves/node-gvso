'use strict';
var assert = require('assert');
var Command = require('../lib/cmds/finishTask');
var GetState = require('')

describe('Finish Task Command Tests', function(){
	/**
	* `gvso commit "Make Work [done #123]"`
	* set 'done' to work item by #id 
	**/	
	it('set "done" to work item by #id', function(done){
		var cmd = new Command({
			auth: {login:'', pass:''},
			task:'123'
		});
		
		cmd.run().then(function(){
			assert.equal(,'Done')
		});
	});
});