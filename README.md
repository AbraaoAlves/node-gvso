
> All the power of Git/VisualStudioOnline in your terminal.

## Table of contents

* [Install](#install)
* [Usage](#usage)
* [Dependencies](#dependencies)
* [Demonstration](#demonstration)
* [Available commands](#available-commands)
    * [Pull requests](#pull-requests)
    * [Notifications](#notifications)
    * [WorkItens](#work)
    * [Repo](#repo)
    * [User](#user)
    * [Alias](#alias)
* [Config](#config)
* [Plugins](#plugins)
* [Tasks](#tasks)
* [Team](#team)
* [Contributing](#contributing)
* [History](#history)
* [License](#license)
