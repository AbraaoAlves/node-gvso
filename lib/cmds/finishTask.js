'use strict';

var BaseCommand = require('./base')
var WebApi = require('vso-node-api/WebApi');

class FinishTaskCommand extends BaseCommand {
	
	constructor(options){
		super(options);
	}
	
	run(){
		
	}
}

module.exports = FinishTaskCommand;