var nopt = require('nopt');
var FinishTaskCommand = require('./cmds/finishTask')

function loadCommand(name){
    //TODO: implement findCommand
    
    return FinishTaskCommand;
}


/**
 * Setup command
 */
exports.setUp = function(){
	var parsedOptions = nopt(process.argv);
    var commandName = parsedOptions[0];
    var Command = loadCommand(commandName); 
    var options = nopt(Command.DETAILS.options, 
        Command.DETAILS.shorthands, process.argv, 2);
    
    return new Command(options);
}

exports.run = function(){
	//TODO: config envioriment
		
	try {
        this.setUp().run();
    } catch (e) {
        console.error(e.stack || e);
    }
};